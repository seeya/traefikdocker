echo "Setting acme permission"
chmod 600 acme.json
echo "Creating network"
docker network create web

echo "What is the domain to watch for? example.com"
read domain

echo "What is your email?"
read email

echo "Updating settings"

sed -i -e "s/CHANGEDOMAIN/${domain}/g" traefik.toml
sed -i -e "s/CHANGEEMAIL/${email}/g" traefik.toml

docker-compose up --build -d

echo "Traefik should be up, run your container now"